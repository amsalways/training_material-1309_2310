CREATE TABLE DEPT1(DEPTNO NUMBER(2) CONSTRAINT DEPTNO_PK PRIMARY KEY,
		   Dname Varchar2(20) Constraint Dname_Nn Not Null,
		   Loc Varchar2(20) Constraint loc_chk Check(Loc In('PUNE', 'MUMBAI', 'DELHI')));
       
CREATE TABLE EMP1(EMPNO NUMBER(3) CONSTRAINT EMPNO_PK PRIMARY KEY,
		  ENAME VARCHAR2(20) CONSTRAINT ENAME_NN NOT NULL,
		  JOB VARCHAR2(10) DEFAULT 'CLERK',
		  DOJ DATE, MGRNO NUMBER(3),
                  SAL NUMBER(8,2) CHECK(SAL > 0),
		  DEPTNO NUMBER(2) REFERENCES DEPT1(DEPTNO),
		  Constraint Mgrno_Fk Foreign Key(Mgrno) References Emp1(Empno));
      
Insert Into Dept1 Values(10, 'ACCOUNTS', 'MUMBAI');      
Insert Into Dept1 Values(&dno, '&DEPTNAME', '&LOCATION');

SELECT *FROM DEPT1;
       
Insert Into Emp1 Values(&eno, '&EMPNAME', '&JOB', '&DATEOFJOIN', Null, &salary, &dno);
Insert Into Emp1 Values(&eno, '&EMPNAME', '&JOB', '&DATEOFJOIN', &mgrno, &salary, &dno);


SELECT * FROM EMP1;

SELECT * FROM EMP;

Select Ename, Job, Sal, Comm, Sal + NVL(Comm, 0) From Emp;

Select Ename EMP_NAME, Job, Sal, Comm, Sal + NVL(Comm, 0) AS TOTAL_INCOME From Emp;

Select Ename Emp_Name, Job, Sal, Comm, Sal + Nvl(Comm, 0) As "TOTAL INCOME" From Emp;

/* SMITH IS CLERK AND GETS 800
   ALLEN IS SALESMAN AND GETS 1600
    ....*/
    
Select Ename || ' IS ' || Job || ' AND GETS ' || Sal "EMP. DETAILS" From Emp;     

Select Concat(Ename , Concat(' IS ', Concat(Job, Concat(' AND GETS ', Sal)))) "EMP. DETAILS" From Emp;

SELECT DISTINCT JOB, DEPTNO FROM EMP;

Select  Job, Distinct Deptno From Emp;

Select * From Emp Where Sal > 2500;

Select * From Emp Where JOB = 'CLERK';

Select * From Emp Where Upper(Job) = Upper('&job');

-- List those who joined after 1st june 82.

Select * From Emp Where Hiredate > '01-JUN-1982';

Select * From Emp Where HIREDATE > '01-06-1982';

Select * From Emp Where Hiredate > To_Date('01-06-1982', 'DD-MM-YYYY');

SELECT * FROM EMP WHERE JOB = 'CLERK' OR JOB = 'MANAGER' AND SAL > 2500;

Select * From Emp Where (Job = 'CLERK' Or Job = 'MANAGER') And Sal > 2500;

Select * From Emp Where (Job = 'CLERK' Or Job = 'MANAGER') And Sal > 2500;

Select * From Emp Where Job In ( 'CLERK' , 'MANAGER') And Sal > 2500;

Select * From Emp Where Sal Between 2000 And 5000;

Select * From Emp Where Sal Between 5000 And 2000;

-- List those who joined in the year 1987

Select * From Emp Where Hiredate Between '01-jan-1987' And '31-dec-1987';

Select * From Emp Where Ename Like 'A%';

Select * From Emp Where Ename Like '%E';

Select * From Emp Where Ename Like '%L%';

Select * From Emp Where Ename Like '%*%%' Escape '*';

Select * From Emp Where Hiredate Like '%87';

Select * From Emp Where Ename Like '%E_';

Select * From Emp Where Ename Like '____';

Select * From Emp Where Length(Ename) = 4;

Select * From Emp Where COMM = NULL;

Select * From Emp Where COMM IS NULL;

Select * From Emp Where Comm IS NOT Null;

Select * From Emp;

SELECT ROWID, ENAME, JOB, SAL FROM EMP;

Select  Ename, Job, Sal From Emp Order By Ename;

Select  Ename, Job, Sal From Emp Order By JOB, SAL, ENAME;

Select Ename Emp_Name, Job, Sal, Comm, Sal + Nvl(Comm, 0) As Total_Income 
   From Emp
   WHERE Sal + Nvl(Comm, 0) > 2500
   ORDER BY TOTAL_INCOME;

Select Ename Emp_Name, Job, Sal, Comm, Sal + Nvl(Comm, 0) As Total_Income 
   From Emp
   Where Sal + Nvl(Comm, 0) > 2500
   Order By 2, 3;
   
Select Sysdate From Dual;

Desc Dual
Insert Into Dual Values('A');
Select * From Dual;

Select 3453454 * 3433 From Dual;

-- MONDAY, 4TH OCTOBER, 2021  12:40:30 PM

Select To_Char(Sysdate, 'FMDay, DDTH Month, YYYY  HH:MI:SS AM') From Dual;

Select * From Emp Where To_Char(Hiredate, 'YYYY') = '1987';

Select Ename, Hiredate, To_Char(Hiredate, 'DAY') "DAY OF JOINING" From Emp;

SELECT SUM(SAL), ROUND(AVG(SAL)), MIN(SAL), MAX(SAL), COUNT(EMPNO) FROM EMP;

Select DEPTNO, Sum(Sal), COUNT(EMPNO)
   From Emp
   GROUP BY DEPTNO;

Select DEPTNO, JOB, Sum(Sal), COUNT(EMPNO)
   From Emp
   Group By Deptno, Job;
   
Select DEPTNO, Sum(Sal), COUNT(EMPNO)
   From Emp
   Group By Deptno
   HAVING COUNT(EMPNO) > 3;

Select DEPTNO, Sum(Sal), COUNT(EMPNO)
   From Emp
   Group By Deptno
   Having Count(Empno) > 3;

Select DEPTNO, Sum(Sal), COUNT(EMPNO)
   From Emp
   Group By Deptno;

Select Ename, Max(Sal) From Emp
  GROUP BY ENAME;

Select Ename, Sal From Emp
   Where Sal = (Select Max(Sal) From Emp);
   
Select Ename, Sal From Emp
   Where Sal =  Max(Sal);

Select Deptno, Sum(Sal)
   FROM EMP
  Group By Deptno
  HAVING SUM(SAL) = (Select MAX(Sum(Sal))
                                    From Emp
                                    Group By Deptno);
 --I-E  
Select E.Ename, E.Sal, D.Dname
  FROM EMP E JOIN DEPT D ON(E.DEPTNO = D.DEPTNO);

-- O-E
Select E.Ename, E.Sal, D.Dname
  From Emp E RIGHT OUTER Join Dept D On(E.Deptno = D.Deptno);

-- O-E
Select E.Ename, E.Sal, D.Dname
  From Emp E LEFT Outer Join Dept D On(E.Deptno = D.Deptno);

-- O-E
Select E.Ename, E.Sal, D.Dname
  From Emp E FULL Outer Join Dept D On(E.Deptno = D.Deptno);

Select E.Ename, E.Sal, S.Grade
  FROM EMP E JOIN SALGRADE S ON(E.SAL BETWEEN S.LOSAL AND S.HISAL);

Select E.Ename, E.Sal, D.Dname, S.Grade
  From Dept D RIGHT OUTER Join Emp E On(E.Deptno = D.Deptno)
                      JOIN SALGRADE S ON(E.SAL BETWEEN S.LOSAL AND S.HISAL);

Select E.Ename "EMPLOYEE", E.MGR, M.EMPNO, M.Ename "MANAGER"
  From Emp E Join Emp M On(E.Mgr = M.Empno );
  
-- List those who get more than CLARK
Select E.eName 
   From Emp E Join Emp C On (E.Sal > C.Sal)  Where C.Ename='CLARK';

Select Ename, Sal  
  FROM EMP WHERE SAL > (SELECT SAL FROM EMP WHERE ENAME = 'CLARK');

Select Ename, Sal  
  FROM EMP WHERE SAL > (SELECT SAL FROM EMP WHERE JOB = 'MANAGER');

Select Ename, Sal  
  From Emp Where Sal > (Select MIN(Sal) From Emp Where Job = 'MANAGER');

Select Ename, Sal  
  From Emp Where Sal >Any (Select Sal From Emp Where Job = 'MANAGER');

Select Ename, Sal  
  From Emp Where Sal IN (Select Sal From Emp Where Job = 'MANAGER');

Select Ename, Sal  
  From Emp Where Sal =ANY (Select Sal From Emp Where Job = 'MANAGER');
  
Select Rownum, Ename, Sal From (Select Ename, Sal From Emp Order By Sal Desc)
   Where Rownum <= 3;

Select Ename, Sal 
   From (Select Rownum Rank, Ename, Sal From (Select Ename, Sal From Emp Order By Sal Desc))
   WHERE RANK = 2;
--   Where Rownum = 2;   
   
/*  Max_sal     Diff.
      -----       ------
         5000       2000  */
         
Select (SELECT Max(Sal) FROM EMP) "MAX_SAL",
            (SELECT MAX(SAL) FROM EMP) -  (Select Sal From (Select Rownum Rank, Ename, Sal 
                                    From (Select Ename, Sal From Emp Order By Sal Desc)) WHERE RANK = 2) "DIFF."
FROM DUAL;         

Select Ename, Sal From Emp E
   WHERE SAL > (SELECT AVG(SAL) FROM EMP WHERE DEPTNO = E.DEPTNO);

Select Job From Emp Where Deptno In(20, 30);

Select Job From Emp Where Deptno = 20
  Union
Select Job From Emp Where Deptno = 30;

Select Job, MGR From Emp Where Deptno = 20
  Union
Select Job, MGR From Emp Where Deptno = 30 ;

Select Job, MGR, DEPTNO From Emp Where Deptno In(20, 30);

Select Job, Mgr From Emp Where Deptno = 20
  Union ALL
Select Job, Mgr From Emp Where Deptno = 30 ;

Select Job From Emp Where Deptno = 20
  Intersect
Select Job From Emp Where Deptno = 10;

Select Job From Emp Where Deptno = 20
Minus
Select Job From Emp Where Deptno <> 20;

Select E.Ename, E.Sal, D.Dname From Emp E Left Outer Join Dept D On(D.Deptno = E.Deptno)
  Union
Select E.Ename, E.Sal, D.Dname From Emp E Right Outer Join Dept D On(D.Deptno = E.Deptno);

CREATE VIEW V1 AS SELECT EMPNO, ENAME, JOB, SAL FROM EMP;

SELECT * FROM V1;

Insert Into V1 Values(9999, 'DIPALI', 'MANAGER', 10000);

UPDATE V1 SET SAL = 3300 WHERE ENAME = 'FORD';

Create Table Emp_Copy As Select * From Emp;

SELECT * FROM EMP WHERE DEPTNO = 10;

CREATE INDEX DEPTNO_IDX ON EMP(DEPTNO);

Select * From Emp_COPY Where Deptno = 10;









